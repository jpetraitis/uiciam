console.log("LDAP server script starting now...");
const ldapjs = require('ldapjs');
const assert = require('assert');

const ldapOptions = {
    url: "ldap://ldap.forumsys.com",
    connectTimeout: 3500,   
    reconnect: true
}

let search = (searchUID) => {
    var opts = {
        filter: `(uid=${searchUID})`,
        scope: 'sub',
        attributes: ['dn', 'sn', 'cn', 'mail', 'telephoneNumber']
    };
    const ldapClient = ldapjs.createClient(ldapOptions);
    return new Promise(function (resolve, reject) {
        //do async call
        ldapClient.search(
            'dc=example,dc=com', 
            opts, 
            (err, res) => {
                assert.ifError(err);
                res.on('searchReference', function(referral){
                    console.log('referral: ' + referral.uris.join());
                })
                res.on('searchEntry', function(entry) {
                    resolve(entry.object);
                });
                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                    if(!ldapClient.connected){
                        return new ldapjs.OperationsError({Error: "Error, ldapclientjs not connected!"})
                    }
                    reject(err);
                    return new ldapjs.ProtocolError(err.toString());
                });
                res.on('end', function(result) {
                    console.log('status: ' + result.status);
                    resolve(result);
                ldapClient.unbind();
                });
            }
        );
    });
};
module.exports = {search: search};