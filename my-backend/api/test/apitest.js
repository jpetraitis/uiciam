let chai = require('chai');
let chaiHttp = require('chai-http');
var should = chai.should();
chai.use(chaiHttp);
let server = require('../server.js');

// parent block
describe('LDAP backend', () => {
    describe('/GET search', () => {
        it('it should GET a search', (done) => {
            chai.request(server)
            .get('/search')
            .end((err, res) => {
                (res).should.have.status(200);
                (res.body).should.be.a('object');
                done();
            });
        });
    });
});