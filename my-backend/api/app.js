var createError = require('http-errors');
var timeout = require('connect-timeout');
var express = require('express');
var cors = require('cors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var app = express();
app.use(timeout('4s'));
app.use(haltOnTimedOut);
function haltOnTimedOut(req, res, next) {
  if(!req.timedout)
    next();
};
// Set up a whitelist and check against it:
var whitelist = ['http://localhost:3000'];
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    } 
  }
}
function logErrors(err, req, res, next) {
  console.error(err.stack);
  next(err);
}
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Then pass them to cors:
app.use(cors());
// having long issue with cors, I think an error case is not being handled in my async request to ldap
app.use('/', indexRouter);
app.use(logErrors);

function errorHandler(err, req, res, next) {
  res.status(500);
  res.send({error: err});
}
// catch 404 and forward to error handler
app.use(errorHandler);


module.exports = app;
