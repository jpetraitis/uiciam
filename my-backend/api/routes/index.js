let express = require('express');
let router = express.Router();
const promisedldap = require('../lib/promisedldapserver');

router.get('/search/:uid', function(req, res, next) {
    console.log('This is search by uid');
    let myPromise = promisedldap.search(req.params.uid);
    myPromise.then(function(result){
        res.send(result);
    })
    .catch(next)
});

module.exports = router;