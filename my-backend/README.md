# README

## Backend for UIC IAM's LDAP Assignment
### This is the server component 

### Description
This NodeJS app is the express backend app serving up RESTful responses.
It makes a call out to an ldap server I have predefined in the code.
ldap.forumsys.com
The /search route is the only route that is available at http://localhost:3001
It takes a url parameter called uid that can be passed like so: http://localhost:/3001/search/eggzamp13
When that happens, the express passes that info to middleware that makes the magic call out to ldap behind the scenes. What is returned is an object that contains the canonical name, distinguished name, surname, email, and
the telephone number of the resulting entry. If there is no entry there is an error message sent back to the user. 

### TODO
- add production vs local .config files
- add tests
- better error handling

_Before you get started, read these REQUIREMENTS_
You will have to have node version 8 or greater to run this app.

Here is how you can get started using the backend

```
    cd api
    npm install
```

Then you can run the app by running

``` 
    cd api
    npm start
```

You can run tests for the app by doing

``` 
    cd api
    npm test
```