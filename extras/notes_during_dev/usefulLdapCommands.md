```
    ldapsearch -LLL -s one -b "c=US" "(o=University*)" o description
```

That command will perform a one-level search at the c=US level for all entries whose organization name (o) begins begins with University. The organization name and description attribute values will be retrieved and printed to standard output, resulting in output similar to this: 

```
    dn: o=University of Alaska Fairbanks,c=US
    o: University of Alaska Fairbanks
    description: Preparing Alaska for a brave new yesterday
    description: leaf node only

    dn: o=University of Colorado at Boulder,c=US
    o: University of Colorado at Boulder
    description: No personnel information
    description: Institution of education and research
```