Man I am so sick of reading Medium articles.

I have some ideas about Express that I want to write down here.
1. Async / await over everythang
2. But I have to wait and do the next

God some of these medium articles are so bad.

This guy marries 
try catch blocks for async await
and promises so you can more specifically pinpoint your error handling.

Doing some more research I found some great tips at scotch io on complicated routing and best practices:
[https://scotch.io/tutorials/keeping-api-routing-clean-using-express-routers](Clean API Routing in Express)

I have this sinking feeling that I can't query the ldap server because I either
A) Don't know how to limit the query results so I'm not getting spooled a huge response (thats not even how that works I dont think, wouldnt it be streamed in?)
B) I dont have an account at uic and all these folks who use ldapsearch have been using dn's that are their normal accounts and logging in and that seems to work. 

I found a great website to simulate a backend with fake data for your frontend:
https://reqres.in/

I also found an odd lateral example for async await in this Node.js Express example tutorial including this technology called Hapi. I don't really care about Hapi or view rendering in express, but this tutorial has good example of async await use in express:
[https://scotch.io/tutorials/build-a-secure-nodejs-application-with-javascript-async-await-using-hapi](Async Await Hapi Express example)
That previous example also has a great quick tutorial on Okta account integration and registration as well as great best practice standards on dotenv files and environment management.

And of course, another one of these great tutorials helps me make a breakthru in my confused thinking about the trycatch/async business I have regarding the search.

Oops these are general javascript ideas for standards I have:
1. Async await over everything
2. let and const when available
3. not too many anonymous functions
4. use your closures well

