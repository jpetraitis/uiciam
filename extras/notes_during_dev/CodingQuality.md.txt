Coding Quality benchmarks to keep in mind

- Good READMEs
- Keep it SOLID and DRY
- TDD, unit tests, acceptance tests
- fix all CSS classes so they have BEM-naming
- have nice error messages, pop up alerts
- loading bars and spinners
- sass compiles down to css (if it is a big project)