ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
Got this error when i tried my first ldap command:

```
    ldapsearch -LLL -h ldap.uic.edu -x -s sub -b "ou=People,dc=uic,dc=edu" "uid=richwolf" 
```

Then I lowercased the P in People and still got the same message.
This was tried from an Ubuntu Server at IP Address 45.33.8.216

Not sure what the issue could be. Preliminary searches on duckduckgo turn up that port 389 might be being blocked.

A thought I had was perhaps I need to be on UIC's network for this to work, but nothing says this on UIC's LDAP information/status page, located here:
[https://accc.drupal.uic.edu/service-status/ldap](LDAP at UIC)

So to help with troubleshooting I looked up an LDAP server that was public that I could hit.
And thats what I found, a public ldapsearch I could do. Here it is:

```
    ldapsearch -W -h ldap.forumsys.com -D "uid=tesla,dc=example,dc=com" -b "dc=example,dc=com"
```
