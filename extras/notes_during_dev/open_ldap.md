Open Ldap research done on 11/20/2019 as part of project for uic.

Going to have to contact ldap.uic.edu with my queries.
I don't know how to form proper queries yet.

I need the ou, cn, etc explained for me.

Here are some links that I had bookmarked regarding this subject:

This link teaches you how to install OpenLDAP on a Windows install.
[https://github.com/cristal-ise/kernel/wiki/Install-OpenLDAP](Install OpenLDAP on Windows)

This link explains how to do OpenLDAP on an Ubuntu server.
[https://help.ubuntu.com/lts/serverguide/openldap-server.html](Install OpenLDAP on Ubuntu)

I found an NPM package that acts as an LDAP client. You pass it the URL, some parameters, and it has options to login if you have one. After that, you can perform searches, results are returned as an array of one or more objects. You can read more about that here:
[https://www.npmjs.com/package/ldap-client](LDAP Client)

LDAP Terms Explained
```
    c = Country
    cn = common name
    dc = Domain Component
    DIT = Directory Information Tree
    DN = Distinguished Name
    l = Location
    LDIF = LDAP Data Interchange Format
    o = Organization
    OID = Object Identifier
    ou = Organizational Unit
    RDN = Relative Distinguished Name
    sn = Surname
    st = State
    structural (object classes) = Each directory entry (instance) contains at least one structural class. Uses inheritance and must be a subclass of some other object class.
    uid = User ID
```

I got the answers for some of these terms from this one blog post I found. Another person named Cody had already defined a bunch of the terms and posted them in a glossary on his website. Here is the page:
[https://codyburleson.com/glossary-of-ldap-acronyms-and-terms/](LDAP Glossary)

I ran into an issue where I couldn't contact the ldap.uic.edu server. I checked the status pages and it was up and running. I reformed my query a few times and got the same results. Then I decided to hit a public ldap server at ldap.forumsys.com and got positive results from there. Then I took fewer options from the command until it worked. It worked being that I got "No result found" for 'richwolf' uid. So, back to the drawing board. Decided to do more reading on what LDAP is and how it works, getting a better handle on these terms above.

I found the online book-ish thing called LDAP for Rocket Scientists. Here it is:
[www.zytrax.com/books/ldap/ch2/index.html#history](LDAP for Rocket Scientists)

I also heavily read the Linux manpage for ldapsearch.
[https://linux.die.net/man/1/ldapsearch](LDAPsearch linux manpage)

I found a possible answer to my problem, my server I am running this ldapsearch command from is having its SSL wildcarded, so that could be causing the issue. Saw the related answer here:
[https://serverfault.com/questions/579131/some-systems-cannot-connect-to-ldap-via-ldaps-but-others-can-is-it-the-wildcar](Wildcard causing error)

This is the error I am receiving:
```
    ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
```

Another good LDAP guide:
[http://ldapjs.org/guide.html](LDAP JS Guide to LDAP)

LDAP Client is the name of the npm module I want to use in my ReactJS app for this
[https://www.npmjs.com/package/ldap-client](LDAP Client)

I just tried getting the uic ldap to work again to no avail. :(
    Going to run ahead and do the project with the forumsys ldap example instead.

I'm trying this blog tutorial on installing ldapsearch for windows:
[http://technololgic.blogspot.com/2013/08/ldapsearch-in-windows.html](Technologic on LDAP search in windows)

LDAP Filters are great at helping define tighter searches:
[https://ldap.com/ldap-filters/](LDAP Filters)

More physical notes:
- openLDAP is a good tool to use as a database for an Address book, list of emails, or mail servers configuration
- You can install LDAP on Windows using Cygwin
- To install on Ubuntu you would install a server daemon called "slapd" and management tools inside "slapd-utils"
- Root name - base or suffix depending on the document, its author, day of the week, or some other variable unknown to us. The top entry in a LDAP DIT
- Data is represented as a hierarchy of object each of which is called an entry. the resulting tree structure is called a Directory information tree.
- Each entry in the tree has one parent entry (object) and zero or more child entries (object). Each child entry (object) is a sibling of its parents' other child entries
- Each entry is composed of an instance of one or more object classes
- Object classes contain zero or more attributes 
- Attributes have names and typically contain data
