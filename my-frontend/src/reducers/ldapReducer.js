import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function ldap(state = initialState.ldap, action) {
    switch(action.type) {
        case types.FETCH_LDAP_SEARCH_SUCCESS:
            return {
                ...state,
                ...action.ldap
            }
        case types.FETCH_LDAP_SEARCH_ERROR:
            return {
                ...state,
                ...action.error
            }
        default: 
            return state;
    }
}