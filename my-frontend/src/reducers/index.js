import { combineReducers } from 'redux';
import ldap from './ldapReducer';

const rootReducer = combineReducers({
    ldap
})

export default rootReducer;