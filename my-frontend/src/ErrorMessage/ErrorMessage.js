import React, { Component } from 'react';
import "./ErrorMessage.css";

class ErrorMessage extends Component {
    constructor(props){
        super(props);
        this.state = {
            ldap: {
                error: 'Default erra message'
            }
        }
    }

    render() {
        if(this.props.ldap.error.length > 0)
            return(
                <div className="ErrorMessage-Container">
                    <div className="ErrorMessage">
                        <span className="ErrorMessage-contents">
                            {this.props.ldap.error}
                        </span>
                    </div>
                </div>
            )
        else
            return(
                <div className="nullError">
                    
                </div>
            )
    }
}

ErrorMessage.defaultProps = {
    ldap: {
        error: "This is a default error"
    }
}

export default ErrorMessage;