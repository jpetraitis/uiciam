class ldapApi {
    static async ldapSearchBy(uid) {
        console.log(JSON.stringify(uid));
        try {
            const data = await this.fetchSearchData(`http://localhost:3001/search/${uid}`);
            console.log(JSON.stringify(data));
            return data;
        } catch (error) {
            console.error(error);
            return error;
        }
    }

    static async fetchSearchData(url = '') {
        const response = await fetch(url, {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'omit',
            headers: {
                'Accept': 'application/json'
            },
            redirect: 'follow',
            referrer: 'no-referrer'
        });
        return await response.json();
    }
}

export default ldapApi;