import React from 'react';
import {bindActionCreators} from 'redux';
import * as ldapActions from '../actions/ldapActions';
import OpenSearch from './OpenSearch';
import "./OpenSearch.css";
import {connect} from 'react-redux';


class OpenSearchContainer extends React.Component {
    constructor(props) {
      super(props);
      this.state = {ldap: null};
    }
    render() {
        return (
            <div className="OpenSearchContainer-component">
                <hr/>
                <OpenSearch {...this.props} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {ldap: state.ldap}
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators(ldapActions, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(OpenSearchContainer);
