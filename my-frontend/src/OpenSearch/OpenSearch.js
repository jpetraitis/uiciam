import React, { Component } from 'react';
import OpenSearchResults from './OpenSearchResults';
import ErrorMessage from '../ErrorMessage/ErrorMessage';

class OpenSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: '',
            ldap: {dn:'',sn:'',cn:'',telephoneNumber:'',mail:''}
        }
        this.submitMySearch = this.submitMySearch.bind(this);
    }
    submitMySearch = event => {
        this.props.actions.ldapSearchBy(this.state.uid);
    }
    componentDidUpdate() {
        console.log(this.state);
        console.log(this.props);
    }
    render () {
        return (
            <div className="OpenSearch-container">
                <div className="Utility-container">
                    <h1>Search UIC LDAP</h1>
                    <div className="searchTerms-SearchBox">
                        <label htmlFor="uidBox">Uid:</label><input name="uidBox" type="text" value={this.state.uid} onChange={(event)=>this.setState({uid: event.target.value})} />
                    </div>
                    <ErrorMessage {...this.props} />
                    <div className="bottomRight-SearchBox">
                        <button className="searchButton" type="submit" onClick={this.submitMySearch}>Submit</button>                    
                    </div>
                </div>
                <OpenSearchResults {...this.props}/> 
            </div>
        );
    }
}

export default OpenSearch;