import React from 'react';
import ReactDOM from 'react-dom';
import OpenSearch from './OpenSearch';
import renderer from 'react-test-renderer';
import * as props from '../reducers/initialState';

const newprops = props.default;
it('renders OpenSearch without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<OpenSearch {...newprops}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should match render from before', () => {
  const tree = renderer
    .create(<OpenSearch {...newprops} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});