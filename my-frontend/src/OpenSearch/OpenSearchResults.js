import React, { Component } from 'react';
import './OpenSearchResults.css';

class OpenSearchResults extends Component {
    render() {
        return (
            <div className="OpenSearch-search-results">
                <div className="Results-BorderBox">
                    <p><label>Distinguished Name:</label><span>{this.props.ldap.dn}</span></p>
                    <p><label>Canonical Name:</label><span>{this.props.ldap.cn}</span></p>
                    <p><label>Surname:</label><span>{this.props.ldap.sn}</span></p>
                    <p><label>Telephone Number:</label><span>{this.props.ldap.telephoneNumber}</span></p>
                    <p><label>Mail:</label><span>{this.props.ldap.mail}</span></p> 
                </div>
            </div>
        )
    }
}

export default OpenSearchResults;