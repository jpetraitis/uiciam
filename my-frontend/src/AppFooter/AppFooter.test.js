import React from 'react';
import ReactDOM from 'react-dom';
import AppFooter from './AppFooter';
import renderer from 'react-test-renderer';

it('renders AppFooter without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AppFooter />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should match render from before', () => {
  const tree = renderer
    .create(<AppFooter />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
