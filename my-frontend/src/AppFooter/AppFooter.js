import React, { Component} from 'react';
import "./AppFooter.css";

class AppFooter extends Component {
    render() {
        return(
            <div className="AppFooter-container">
                <div className="AppFooter-subcontainer">
                    <div className="AppFooter-explain">
                        <p>An open-source LDAP Search app written for UIC</p>
                        <p>Learn more about <a href="/">this project</a>.</p>
                        <p>Read this project's <a href="https://bitbucket.org/jpetraitis/uiciam/">source code</a>.</p>
                        <p><em>by Jack Petraitis</em></p>
                        <p>Learn more about <a href="https://jackpetraitis.com">him</a>.</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default AppFooter;