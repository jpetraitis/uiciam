import React, { Component} from 'react';
import "./AppHeader.css";

class AppHeader extends Component {
    render() {
        return(
            <div className="AppHeader-container">
                <div className="AppHeader-subcontainer">
                    <div className="AppHeader-link">
                        UIC's Open LDAP Search
                    </div> 
                    <div className="AppHeader-link">
                        <a href="/">Homepage</a>
                    </div>
                    <div className="AppHeader-link">
                        <a href="/opensearch">Opensearch</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default AppHeader;