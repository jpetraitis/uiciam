import React from 'react';
import ReactDOM from 'react-dom';
import AppHeader from './AppHeader';
import renderer from 'react-test-renderer';

it('renders AppHeader without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AppHeader />, div);
  ReactDOM.unmountComponentAtNode(div);
});


it('should match render from before', () => {
  const tree = renderer
    .create(<AppHeader />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});