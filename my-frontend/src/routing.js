import React from "react";
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import HomePage from "./HomePage/HomePage";
import AppHeader from "./AppHeader/AppHeader";
import AppFooter from "./AppFooter/AppFooter";
import OpenSearchContainer from "./OpenSearch/OpenSearchContainer";

function Routing() {
    return (
        <Router>
            <div className="App-Container">
                <AppHeader/>
                <Route exact path="/" component={HomePage} />
                <Route path="/opensearch" component={OpenSearchContainer} />
                <AppFooter/>
            </div>
        </Router>
    )
}

export default Routing;