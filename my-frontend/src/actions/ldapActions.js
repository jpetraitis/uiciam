import * as types from './actionTypes';
import ldapApi from '../api/ldapApi';

function submitSearchRequestSuccess(payload) {
  if(payload.messageID){
    // this is the case where the ldap middleware found no result
    var errorPay = {
      error: "Could not find an entry on backend",
      dn: '',
      cn: '',
      sn: '',
      telephoneNumber: '',
      mail: ''
    }
    return {type: types.FETCH_LDAP_SEARCH_ERROR, error: errorPay}
  }else if(payload.error){
    var errorPay = {
      error: payload.error.message
    }
    return {type: types.FETCH_LDAP_SEARCH_ERROR, error: errorPay}
  }else{
    var oldPayload = payload;
    var noError = {
      error: ""
    }
    var newPayload = {
      ...oldPayload,
      ...noError
    }
    return {type: types.FETCH_LDAP_SEARCH_SUCCESS, ldap: newPayload}
  }
}

function submitSearchRequestFailure(error){
  return {type: types.FETCH_LDAP_SEARCH_ERROR, ldap: error}
}

export function ldapSearchBy(input) {
  return function(dispatch) {
    return ldapApi.ldapSearchBy(input)
      .then(returnPayload => {
        dispatch(submitSearchRequestSuccess(returnPayload))
      })
      .catch(error => {
        console.error(error);
        dispatch(submitSearchRequestFailure(error));
      })
  }
}
