import React, { Component } from 'react';
import logo from '../logo.svg';
import node from '../node.svg';
import "./HomePage.css";

class HomePage extends Component { 
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logos" alt="logo" />
                    <img src={node} className="App-logos" alt="node" />
                    <em>This app written by Jack Petraitis</em>
                    <em>Uses ReactJS and NodeJs</em>
                    <h2>App Explanation...</h2>
                    <div className="AppExplanation">
                    This app needed to reach out to ldap.uic.edu and pull back information from queries.
                    The backend was already there for my usage, I just needed to learn how to use it by reading any docs I could find.
                    Then I could build queries for it using NodeJs and send out requests using middleware for my searches. Any data that comes back gets populated in the ReactJS app.
                    </div>
                    <h2>Problems that arose...</h2>
                    <div className="AppExplanation">
                    Throughout this assignment I tackled many problems. Many common ones that I had seen before and could fix quickly, such as cors.
                    There were some that I was a bit more perplexed on and would have cost me more time had I not made certain key breakthroughs, whether by
                    noticing a small mistake or correcting my code's configuration after reading the manual. There was one issue I could never surmount, and that
                    was my inability to get data back from UIC's ldap...for some reason. Even when I used ldapsearch from a linux terminal from multiple geographic 
                    locations I couldn't get a peep from the service. It just hung for 30 seconds and then timed out. The only thing I could think of is that I used the 
                    service wrong, so I took copious notes on ldap and learned along the way how it worked. I included those in the submission. And I used another public
                    ldap service as the replacement during my testing. Another issue I had been having is backend stability. It seemed I could only get one or two requests in
                    before the backend quit responding on me. Only thing I could think would be the problem would be the middleware library running the calls out to the ldap 
                    service hitting a snag or an error that I hadn't handled yet.
                    </div>
                </header>
            </div>
        )
    }
}

export default HomePage;