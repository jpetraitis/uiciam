import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from './HomePage';
import renderer from 'react-test-renderer';

it('renders HomePage without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<HomePage />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should match render from before', () => {
  const tree = renderer
    .create(<HomePage />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});