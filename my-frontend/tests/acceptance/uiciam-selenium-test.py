# Acceptance test file for UIC IAM OpenLDAP search
# user must be able to navigate homepage/opensearch
# user must be able to complete an opensearch

from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# just dash, projects, schedules, todo smoke test suite
class UicIamOpenSearchAcceptance (unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def test_opensearch_content(self):
        self.driver.get("http://localhost:3000")
        self.opensearch_link = self.driver.find_element_by_link_text('Opensearch')
        self.opensearch_link.click()
        self.assertIn("UIC LDAP Search", self.driver.title)
        self.assertEqual(1,len(self.driver.find_elements_by_name("uidBox")))
        buttons = self.driver.find_elements_by_class_name("searchButton")
        self.assertEqual(1, len(buttons))
        self.assertEqual(1,len(self.driver.find_elements_by_class_name("OpenSearch-search-results")))

    def tearDown(self):
        self.driver.quit()

    def is_element_present(self, how, what):
        """
        Helper method to confirm the presence of an element on page
        :params how: By locator type
        :params what: locator value
        """
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException: return False
        return True

if __name__ == "__main__":
    unittest.main()


#tooken from http://selenium-python.readthedocs.io/getting-started.html#using-selenium-to-write-tests
