# Testing Readme
## For the UIC IAM Open LDAP search project

_by Jack Petraitis_


### Acceptance Tests
To get started running these acceptance tests you have to do some prerequisites.
_Requirements:_
- You must have python3 installed
- You must have selenium installed (a python3 package)

These next steps actually help you complete those requirements
_Steps for setup_
1. Install Python3 - [https://www.python.org/downloads/](Direct Link)
2. pip3 install selenium
(If that doesn't work on your machine like it didn't on my Windows 10 laptop, try this:)
```
    py -m pip install selenium
```

TROUBLESHOOTING
If you get this error:
```
    Message: 'geckodriver' executable needs to be in PATH.
```
Then you have to install the latest version of geckodriver and/or add the executable to your PATH.
To do that you have to download it from the following website:
[https://github.com/mozilla/geckodriver/releases](Latest Geckodriver)
Then, once you extract it, find a good spot for it to live, for me, I store mine in "C:\Utilities\geckodriver\".
That way Selenium can see it when it tries to run.

_Steps to get started_
1. Run the project website (+ the backend) locally 
2. Run the following commands
``` 
    cd acceptance
    python3 uiciam-selenium-test.py
```