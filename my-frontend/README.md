# Readme
## An Open LDAP Search - done in ReactJS ( + NodeJS backend )
_Jack Petraitis_

### Description
This project is an OpenLDAP search interface written in ReactJS and runnable by completing the following commands. This app must be run in tandem with a backend app written in NodeJS. This UI frontend app makes calls
to that backend app and in return is fed back data to populate the app.

_Requirements:_
_NOTE: Be sure to have latest version of nodejs installed before attempting these commands._
```
    npm install
    npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### Running Tests
If you are interested in running the tests for the project you can find them and run them by doing;
```
    npm run test
```

### Running Builds
If you are interested in building the project for production purposes, do the following:
```
    npm run build
```

