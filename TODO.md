# TODO still

## in order of importance

+ Add app.config files
+ Configure jenkins ci/cd for an automatic backend deployment
+ Write the DEPLOY.md documentation
+ Fix server gone away issues
+ Better reactjs error messaging
+ Better express error messaging
+ Experiment with UICs ldap command again, see if it works
+ Modularize & componentize helper logic
+ Add Regression testing for all the possible results of the promise/event loop
+ Add tests [unit, integration/regression, acceptance]
+ Fix any more funny business with the API
+ 